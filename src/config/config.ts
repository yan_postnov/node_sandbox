export const config = {
  PORT: process.env.PORT || 4000,
  MODE: process.env.NODE_ENV,
};

export const isDevelop = process.env.NODE_ENV === "development";
