import mongoose from "mongoose";
import chalk from "chalk";

const mongoUrl: string = "mongodb://localhost:27017/mongo-test";

const connectMongo = (url: string) => {
  mongoose
    .connect(url, { useNewUrlParser: true })
    .then(() => {
      // tslint:disable-next-line:no-console
      console.log(`[mongo]: successfully connected to ${url}`);
    })
    .catch((err) => {
      // tslint:disable-next-line:no-console
      console.log(
        chalk.red(`[mongo]: error while connecting to ${url}`),
        "\n",
        chalk.red(err)
      );
      process.exit(1);
    });
};

export { mongoUrl, connectMongo, mongoose };
