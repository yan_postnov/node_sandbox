import express, { Application } from "express";
import cors from "cors";
import helmet from "helmet";
import morgan from "morgan";
import { isDevelop } from "../config/config";
import { connectMongo, mongoUrl } from "./mongo";

export const ExpressApplication = () => {
  const app: Application = express();

  connectMongo(mongoUrl);
  if (isDevelop) app.use(morgan("dev"));
  app.use(helmet());
  app.use(cors());

  return app;
};
