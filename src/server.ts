import { Request, Response } from "express";
import { ExpressApplication } from "./services/express";
import { config } from "./config/config";

const app = ExpressApplication();

app.get("/", (req: Request, res: Response) => {
  res.status(200).send("WASSUP");
});

app.listen(config.PORT, () =>
  // tslint:disable-next-line:no-console
  console.log(`[server]: listening on http://localhost:${config.PORT}`)
);
